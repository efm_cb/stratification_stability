function calc_dens(sal,t090C)
# Calculates density profile for a 2 layer system
# Surface layer is assumed to be 2-m and total depth 100m
# Inputs: 2 value arrays = [bottom surface] values
#=
* S: salinity in g/kg
* T: temperature in oC
=#
# Returns the handle of the figures



den=limstate.(sal,t090C,[0 0])*1000

diffDen = round(den[1].-den[2], digits=2)

println("Surface is $diffDen kg/m3 lighter than the bottom layer")


# Plotting
zP=-[0 2 2 10]
t = [t090C[2] t090C[2] t090C[1] t090C[1]]
s= [sal[2] sal[2] sal[1] sal[1]]

den=limstate.(s, t , [0 0 0 0])
# sigmaT
den= (den.*1000).-1000
dmin=floor(minimum(den))

x=[t' s' den']


p=plot(x,zP', xlabel = ["T [oC]" "S [g/kg]" "density [kg/m3]"],layout = (1, 3),legend=false)
ylabel!(p[1],"Depth [m]")
title!(p[2],"Surface T = $T oC")
plot!(p[3], xlims = (dmin,6))

return p
end
