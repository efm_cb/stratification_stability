# Readme
I've prepared some material for the demo, which will focus on stratification and its relationship to stability/potential energy in natural environments. The jupyter notebook has some interactive code  for visualising your attempts to  the questions, but its use is optional.  

## Before class checklist

* Please connect to the jupyter notebook:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fefm_cb%2Fstratification_stability/HEAD?filepath=queens_bothdemo.ipynb)

* Aternatively,  simply download the  PDF version from the [repo](queens_bothdemo.pdf) or [dropbox](https://www.dropbox.com/s/vvfhfnchspe8u0s/queens_bothdemo.pdf?dl=0)
* Be prepared to answer one [question](https://forms.gle/Vikh6hCxuLtt26E57) about stratification!




 Head binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fefm_cb%2Fstratification_stability/HEAD)
