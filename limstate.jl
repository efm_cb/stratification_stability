function limstate(S,T,p)
# LIMSTATE is limnological eqn of state
#   rho=limstate(S,T,p) provides the density
#   (kg/l soln) at a
#         S - salinity [g dissolved/kg water] 0-0.6
#         T - temp [deg C]   0-30C
#         p - pressure [dbar]  0-1800 dbars
#
#
#   as per Chen & Millero, Limnol. Oceanogr. 31[3], 1986, 657-662.
#
#   If we enter S<0; then I take this as PSS-78 salinity
#   & compute the seawater eqn. of state as per
#
#    Millero; F.J.; Chen; C.T.; Bradshaw; A.; & Schleicher; K.
#    Deap-Sea Research.; 1980; Vol27A; pp255-264.


# For limnological eqn:
#
# Checkvals limstate(0,10,1000) = 1.0044277
#           limstate(0.5,10,0)  = 1.0000920

# R. Pawlowicz 22/Nov/06




P=p/10;  # pressure in bars


rho0 =   0.9998395 + T.*( 6.7914e-5 + T.*(-9.0894e-6 + T.*( 1.0171e-7  + T.*(-1.2846e-9  + T.*( 1.1592e-11 + T.*(-5.0125e-14)))))) + S.*(    8.181e-4 + T.*(-3.85e-6  + 4.96e-8 .*T)  )

K =  19652.17 + T.*( 148.113 + T.*(-2.293 + T.*( 1.256e-2+ T.*(-4.18e-5 )))) + P.*(3.2726 + T.*(-2.147e-4	+ 1.128e-4.*T))+ S.*(53.238 + -0.313.*T + P.*5.728e-3 )

  rho=rho0./(1- P./K)


return rho
end
