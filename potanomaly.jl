function  potanomaly(rho,H,h1)
# Calculates the potential energy anomalies for a 2-step system
# Requires
#   rho= array of 2 density values (in kg/m3)
#   H = thickness of water colum
#   h1 = thickness of surface layer
# rho=Array{Float64, 2}(undef, 1, 2) # define variable
# rho[1]=1000
# rho[2]=1004

# Variables
g =  -9.81; # gravity

#= function to compute potential energy (constant) between two limits
z0=[surface-most depth, deepest depth] i.e., integration limits. Positive z is upwards
rho0= layer's density of water
=#

potEn=(rho0,z0)->(0.5*g*rho0.*(z0[1].^2 - z0[2].^2)) # J/m2 (need to divide by H)

#= Calculates the average density for well-mixed profile (2-step layer)
rho=  2 values for each var (surface, then bottom values)
z0 = [H, h1]= [total depth, thickness of top layer ]
=#
rhoM =(rhoT,z0) ->((rhoT[2].*(z0[1]-z0[2]) + rhoT[1].*abs(z0[2]))./(abs(z0[1])))



# Initial (stratified) potential energy in the B profile
potTop = potEn(rho[1],[H-h1 H]);
potBot = potEn(rho[2],[0 H-h1]);
potBini= (potBot+potTop)./H


# Final (well-mixed) potential energy in B profile
rhoF = rhoM(rho,[H h1])
potBmixed=potEn(rhoF,[0 H])./H

phiAnomalyB= round(potBmixed - potBini, digits=0)

# Rounding for print
rhoF=round(rhoF, digits=2)
potBini=round(potBini,digits=0)

println("Density of the well-mixed profile is = $rhoF kg/m3 ")
#println("The potential energy of the stratified layer= $potBini J/m3")
println("The potential energy anomaly (stability)  = $phiAnomalyB J/m3 \n")

return phiAnomalyB

end
